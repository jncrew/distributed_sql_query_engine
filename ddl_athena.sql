CREATE EXTERNAL TABLE `default`.`product` (
`ix` bigint,
`product_seq` bigint,
`store_seq` bigint,
`product_title` string,
`product_sub_title` string,
`product_status` tinyint,
`product_hidden_status` tinyint,
`product_description` string,
`product_price` bigint,
`product_cost` bigint,
`product_category` int,
`product_condition` tinyint,
`product_color` string,
`parcel_fee_yn` tinyint,
`consignment_type` tinyint,
`product_trade_type` tinyint,
`rec_cnt` int,
`device_os` tinyint,
`sort_date` timestamp,
`partnercenter_payment_url` string,
`partnercenter_main_yn` tinyint,
`product_section_type` tinyint,
`platform_type` tinyint,
`model_name` string,
`reserve_col2` bigint,
`reserve_col3` string,
`reserve_col4` bigint,
`update_date` timestamp,
`reg_date` timestamp
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe'
WITH SERDEPROPERTIES (
'serialization.format' = ',',
'field.delim' = ','
) LOCATION 's3://joongna-search/ml/jnapp_product/data'
TBLPROPERTIES ('skip.header.line.count'='1','compressionType'='gzip');

CREATE EXTERNAL TABLE `default`.`search_word_log` (
`search_word_log_seq` bigint,
`store_seq` bigint,
`search_word` string,
`search_type` tinyint,
`save_yn` tinyint,
`category_seq` bigint,
`category_depth` tinyint,
`product_condition` tinyint,
`product_condition_full_package_yn` tinyint,
`product_condition_limited_edition_yn` tinyint,
`product_condition_flawed_yn` tinyint,
`min_price` int,
`max_price` int,
`sort_start_date` timestamp,
`sort_end_date` timestamp,
`product_status` tinyint,
`product_color` string,
`sort_order` tinyint,
`sort_date` tinyint,
`sort_price` tinyint,
`product_trade_type` tinyint,
`reg_date` timestamp
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.lazy.LazySimpleSerDe'
WITH SERDEPROPERTIES (
'serialization.format' = ',',
'field.delim' = ','
) LOCATION 's3://joongna-search/ml/jnapp_main_search_word_log'
TBLPROPERTIES ('skip.header.line.count'='1','compressionType'='gzip');