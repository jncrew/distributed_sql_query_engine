
#
# Data Source: @jn-live-db-maria-jn3-r2-004.cruxxnen4hwi.ap-northeast-2.rds.amazonaws.com [2] Schema: jnapp_main Table: product  -- 상품 마스터 정보 - 상품 샘플 입력
# -- auto-generated definition
# create table product
# (
#     product_seq               bigint unsigned auto_increment comment '상품 게시글 일련번호'
#         primary key,
#     store_seq                 bigint unsigned     default 0                   not null comment '상품 게시 store seq',
#     product_title             varchar(100)        default ''                  not null comment '상품 게시글 제목',
#     product_sub_title         varchar(100)        default ''                  not null comment '파트너스센터 상품 게시글 제목',
#     product_status            tinyint(1)          default 0                   not null comment '상품 게시글 상태 (0:판매중, 1:예약중, 2:판매보류, 3:판매완료, 4:상품삭제, 7:판매대기, 8:차고 등록(오토용))',
#     product_hidden_status     tinyint(1)          default 0                   not null comment '상품 노출 상태(0:노출중, 4:삭제, 5:관리자숨김, 6:신고숨김, 7:회원제재숨김, 8:URL로접근)',
#     product_description       text                default ''                  not null comment '상품 상세 설명',
#     product_price             bigint(11) unsigned default 0                   not null comment '상품 가격',
#     product_cost              bigint(11) unsigned default 0                   not null comment '상품 공급가',
#     product_category          int                 default 0                   not null comment '상품 카테고리',
#     product_condition         tinyint(1)          default 2                   not null comment '상품 상태 (0:새것, 1:거의새것, 2:중고)',
#     product_color             varchar(10)         default 'ZZZZZZ'            not null comment '상품 색상',
#     parcel_fee_yn             tinyint(1)          default 0                   not null comment '상품 가격의 택배비 포함 여부 (0:포함, 1:미포함) -> 0:미포함, 1:포함으로 변경함 (TODO : 마이그레이션 후 업데이트 여부 확인)',
#     consignment_type          tinyint(1)          default 0                   not null comment '위탁상품 여부 (0 : 일반상품, 1 : 쇼핑 직매입, 2 : 쇼핑 위탁상품 + 링크결제, 3 : 쇼핑 위탁상품 + PG결제) ',
#     product_trade_type        tinyint(1)          default 0                   not null comment '거래 방법 (0:모두가능, 1:직거래만, 2:택배거래만)',
#     reg_cnt                   int                 default 0                   not null comment '상품 재등록 숫자',
#     device_os                 tinyint(1)          default 9                   null comment 'OS type (0:Android, 1:iOS, 2:mobileWeb, 9:알수없음)',
#     sort_date                 datetime            default current_timestamp() not null comment '상품 게시글 정렬 기준일 (재등록일 반영)',
#     partnercenter_payment_url varchar(500)        default ''                  not null comment '파트너스센터 결제 URL',
#     partnercenter_main_yn     tinyint(1)          default 0                   not null comment '인증상품 메인 노출 여부(0 : 미노출, 1 : 노출)',
#     product_section_type      tinyint(1)          default 0                   not null comment '상품 노출 탭 구분 ( 0 : 해당 없음, 10 : 새상품, 20 : 구제상품, 21: [구제나라], 30 : 중고폰 )',
#     platform_type             tinyint(1) unsigned default 1                   not null comment '사업별 구분 (1 : 중고나라, 2 : 오토,  3 : 파트너센터)',
#     model_name                varchar(100)                                    null comment '상품 모델명 추출 정보',
#     reserve_col2              bigint                                          null comment '예비칼럼 2',
#     reserve_col3              varchar(100)                                    null comment '예비칼럼 3',
#     reserve_col4              bigint                                          null comment '예비칼럼 4',
#     update_date               datetime            default current_timestamp() not null comment '상품 게시글 수정일',
#     reg_date                  datetime            default current_timestamp() not null comment '상품 게시글 등록일'
# )
#     comment '상품 마스터 정보 - 상품 샘플 입력';
#
# create index ix_product_01
#     on product (product_seq, product_status, reg_date);
#
# create index ix_product_02
#     on product (store_seq, product_seq, reg_date);
#
# create index ix_product_03
#     on product (reg_date, product_seq, store_seq);
#
# create index ix_product_04
#     on product (platform_type, product_seq);
#
# create index ix_product_05
#     on product (product_price, product_category, reg_date);
#
# create index ix_product_06
#     on product (product_status, sort_date);
#  Show table preview
import sys, os.path, boto3
from os import path
from awsglue.transforms import *
from awsglue.utils import getResolvedOptions
from pyspark.context import SparkContext
from awsglue.context import GlueContext
from awsglue.job import Job
from pyspark.sql.types import *
from pyspark.sql.functions import *

s3 = boto3.resource('s3')
sc = SparkContext()
conf = sc.getConf()
# add additional spark configurations
conf.set("spark.sql.legacy.parquet.int96RebaseModeInRead", "CORRECTED")
conf.set("spark.sql.legacy.parquet.int96RebaseModeInWrite", "CORRECTED")
conf.set("spark.sql.legacy.parquet.datetimeRebaseModeInRead", "CORRECTED")
conf.set("spark.sql.legacy.parquet.datetimeRebaseModeInWrite", "CORRECTED")
sc.stop()
sc = SparkContext.getOrCreate(conf=conf)
# create glue context with the restarted sc
glueContext = GlueContext(sc)
# glueContext = GlueContext(SparkContext.getOrCreate())

spark = glueContext.spark_session

inputbucket = 'joongna-search/ml/jnapp_product/data'
outputbucket = 'joongna-search/ml/jnapp_product'
outputprefix = 'parquet/'

schema = StructType([
    StructField("idx", IntegerType()),
    StructField("product_seq", IntegerType()),
    StructField("store_seq", IntegerType()),
    StructField("product_title", StringType()),
    StructField("product_sub_title", StringType()),
    StructField("product_status", IntegerType()),
    StructField("product_hidden_status", IntegerType()),
    StructField("product_description", StringType()),
    StructField("product_price", IntegerType()),
    StructField("product_cost", IntegerType()),
    StructField("product_category", IntegerType()),
    StructField("product_condition", IntegerType()),
    StructField("product_color", StringType()),
    StructField("parcel_fee_yn", IntegerType()),
    StructField("consignment_type", IntegerType()),
    StructField("product_trade_type", IntegerType()),
    StructField("rec_cnt", IntegerType()),
    StructField("device_os", IntegerType()),
    StructField("sort_date", TimestampType()),
    StructField("partnercenter_payment_url", StringType()),
    StructField("partnercenter_main_yn", IntegerType()),
    StructField("product_section_type", IntegerType()),
    StructField("platform_type", IntegerType()),
    StructField("model_name", StringType()),
    StructField("reserve_col2", IntegerType()),
    StructField("reserve_col3", StringType()),
    StructField("reserve_col4", IntegerType()),
    StructField("update_date", TimestampType()),
    StructField("reg_date", TimestampType()),
])

df = spark.read.csv('s3://' + inputbucket + '/jnapp_main_product.csv.gz', header=True, schema=schema, quote='"', escape='"')
df.write.parquet('s3://' + outputbucket + '/' + outputprefix + 'jnapp_main_product', compression="snappy", mode="Overwrite")
